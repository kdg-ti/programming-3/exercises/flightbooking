package be.kdg.java2.flightbooking.ui;

import be.kdg.java2.flightbooking.service.FlightBookingService;
import be.kdg.java2.flightbooking.ui.viewmodels.FlightBookingViewModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
public class BookFlightController {
    private FlightBookingService flightBookingService;

    public BookFlightController(FlightBookingService flightBookingService) {
        this.flightBookingService = flightBookingService;
    }

    @GetMapping({"","/index","/"})
    public String getBookFlightPage(Model model){
        model.addAttribute("destinations", flightBookingService.getDestinations());
        model.addAttribute("flightBooking",new FlightBookingViewModel());
        return "index";
    }

    @PostMapping("/bookflight")
    public String bookFlight(FlightBookingViewModel flightBookingViewModel){
        flightBookingService.bookFlight(flightBookingViewModel.getName(), flightBookingViewModel.getDestination(), flightBookingViewModel.getAccount());
        return "redirect:index";
    }
}
