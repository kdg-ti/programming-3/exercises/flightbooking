package be.kdg.java2.flightbooking.exceptions;

public class InvalidAccountException extends RuntimeException{
    public InvalidAccountException(String message) {
        super(message);
    }
}
