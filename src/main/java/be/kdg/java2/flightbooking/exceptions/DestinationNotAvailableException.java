package be.kdg.java2.flightbooking.exceptions;

public class DestinationNotAvailableException extends RuntimeException {
    public DestinationNotAvailableException(String message) {
        super(message);
    }
}
